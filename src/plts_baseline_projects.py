# compare baseline attributes and project attribute and see if the attributes
# got better or worse
from dbUtil import dbUtil
import geopandas as gpd
import config as c
import env
import psycopg2
import pandas as pd
import numpy as np

pd.set_option("display.max_rows", 500)
# pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 400)

qual_columns = [
    "pavement_condition",
    "sidewalk_buffer_type",
    "bicycle_buffer_type",
    "bicycle_path_category",
    "bicycle_path_type",
    "bicycle_approach_alignment",
]
quan_columns = [
    "heavy_vehicle_count",
    "aadt",
    "volume_capacity",
    "sidewalk_width",
    "sidewalk_buffer_width",
    "sidewalk_condition_score",
    "bicycle_buffer_width",
    "bicycle_facility_width",
    "parking_lane_width",
]

plts_cols = [
    "sidewalk_width",
    "sidewalk_buffer_width",
    "sidewalk_condition_score",
]


def clean_future_projects():
    # getting the future projects clean up
    df = gpd.read_file(
        "bike_projects.gpkg", driver="GPKG", layer="BikeProjects_finaledits"
    )
    df = dbUtil.rename_column(df, c.SANITIZE)

    ori = df.filter(regex=("^((?!data-1567615426275).)*$|segment_id"))
    new = df.filter(regex=("data-1567615426275.*|segment_id"))
    new = dbUtil.rename_column(new, c.REMOVE)

    ori = ori.set_index("segment_id")
    new = new.set_index("segment_id")
    ori.update(new)

    ori = ori.reset_index()
    ori.loc[
        ori["pavement_condition"] == "100", "pavement_condition"
    ] = "Excellent"

    to_numeric = [
        "bicycle_buffer_width",
        "bicycle_facility_width",
        "sidewalk_condition_score",
        "sidewalk_buffer_width",
        "sidewalk_width",
    ]
    for col in to_numeric:
        ori = dbUtil.convert_data_type(ori, col, "numeric")

    ori = ori.set_index("segment_id")
    return ori


def get_network(connection, query, geom_col="geom"):
    with psycopg2.connect(**connection) as conn:
        if geom_col:
            network_df = gpd.GeoDataFrame.from_postgis(
                query, conn, geom_col=geom_col
            )
    return network_df


def plts_diff(df):
    plts = df.query(
        "sidewalk_width<0 or sidewalk_buffer_width<0 or sidewalk_condition_score<0"
    )
    plts = plts[plts_cols]
    return plts


def project_name():
    df = gpd.read_file(
        "bike_projects.gpkg", driver="GPKG", layer="BikeProjects_finaledits"
    )
    df = df[['data-1567615426275_name', 'segment_id']]
    df = df.rename(columns={'data-1567615426275_name': 'project_name'})
    df = df.set_index('segment_id')
    return df


def blts_diff(df):
    blts = df.copy()
    return blts


if __name__ == "__main__":
    future = clean_future_projects()
    network = get_network(env.DB, c.NETWORK_SQL)
    project_name = project_name()
    
    future_quan = future[quan_columns]
    network_quan = network[quan_columns]
    network_quan = network_quan.fillna(0)
    diff = future_quan.sub(network_quan, axis="index")
    diff = diff.loc[future.index]
    plts = plts_diff(diff)

    # compare sidewalk buffer
    network = network.replace(np.nan, 'none', regex=True)
    future = future.replace(np.nan, 'none', regex=True)
    future = future.replace('', 'none', regex=True)
    network = network.replace('', 'none', regex=True)
    future = future.replace(' ', 'none')
    network = network.replace(' ', 'none')
    future['sidewalk_buffer_type'] = future['sidewalk_buffer_type'].fillna('none')
    network['sidewalk_buffer_type'] = network['sidewalk_buffer_type'].fillna('none')
    sidewalk_buffer_diff = future.sidewalk_buffer_type.str.lower() != network.loc[future.index].sidewalk_buffer_type.str.lower()

    buffer_diff = network.loc[future.index][sidewalk_buffer_diff]['sidewalk_buffer_type'].str.lower() + '>' + future[sidewalk_buffer_diff]['sidewalk_buffer_type'].str.lower()
    buffer_diff = buffer_diff[~buffer_diff.str.contains('>none')]
    buffer_diff = buffer_diff[~buffer_diff.str.contains('none>')]
    plts = pd.DataFrame(buffer_diff).merge(plts, how='outer', left_index=True, right_index=True)


    # join project name
    plts = plts.merge(project_name, how='left', left_index=True, right_index=True)
    plts = plts.sort_values(by='project_name')
    import pdb; pdb.set_trace()