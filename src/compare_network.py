import geopandas as gpd


def get_destination():
    project_path = "/mnt/data/baseline/pois_access_segment.gpkg"
    layer = "destination"
    gdf = gpd.read_file(project_path, driver="GPKG", layer=layer)
    return gdf


def get_projects():
    project_path = "/mnt/git/snt-scenario-editing/src/bike_projects.gpkg"
    layer = "BikeProjects_finaledits"
    gdf = gpd.read_file(project_path, driver="GPKG", layer=layer)
    gdf = gdf[["data-1567615426275_name", "geometry"]]
    gdf = gdf.rename(columns={"data-1567615426275_name": "name"})
    return gdf


def main():
    base_path = "/mnt/data/baseline/access_data_segment_id.gpkg"
    preferred_path = "/mnt/data/preferred/access_data_segment_id.gpkg"
    preferred_hsr_path = "/mnt/data/preferred_hsr/access_data_segment_id.gpkg"
    projects = get_projects()
    destinations = get_destination()
    # load df
    base_df = gpd.read_file(base_path, driver="GPKG", layer="segment")
    preferred_df = gpd.read_file(
        preferred_path, driver="GPKG", layer="segment"
    )
    preferred_hsr_df = gpd.read_file(
        preferred_hsr_path, driver="GPKG", layer="segment"
    )

    # set index
    base_df = base_df.set_index("segment_id")
    preferred_df = preferred_df.set_index("segment_id")
    preferred_hsr_df = preferred_hsr_df.set_index("segment_id")

    # remove str columns
    base_df = base_df.drop(
        ["id", "cross_name_start", "cross_name_end", "name"], axis=1
    )
    preferred_df = preferred_df.drop(
        ["id", "cross_name_start", "cross_name_end", "name"], axis=1
    )

    # compare the difference
    base_preferred = preferred_df.sub(base_df)
    base_preferred_hsr = preferred_hsr_df.sub(base_df)

    # convert to numeric
    base_preferred = base_preferred.astype(int, errors="ignore")
    base_preferred_hsr = preferred_hsr_df.astype(int, errors="ignore")

    # reset geometry
    base_preferred["geometry"] = base_df["geometry"]
    base_preferred_hsr["geometry"] = base_df["geometry"]

    # set crs
    base_preferred.crs = {"init": "epsg:4326"}
    base_preferred_hsr.crs = {"init": "epsg:4326"}

    # export
    base_preferred_diff_path = "/mnt/data/lrtp/base_preferred_diff.gpkg"
    base_preferred_hsr_diff_path = (
        "/mnt/data/lrtp/base_preferred_hsr_diff.gpkg"
    )
    import pdb

    pdb.set_trace()
    base_preferred.to_file(
        base_preferred_diff_path, layer="network", driver="GPKG"
    )
    base_preferred_hsr.to_file(
        base_preferred_hsr_diff_path, layer="network", driver="GPKG"
    )

    projects.to_file(base_preferred_diff_path, layer="project", driver="GPKG")
    projects.to_file(
        base_preferred_hsr_diff_path, layer="project", driver="GPKG"
    )
    destinations.to_file(
        base_preferred_diff_path, layer="destination", driver="GPKG"
    )
    destinations.to_file(
        base_preferred_hsr_diff_path, layer="destination", driver="GPKG"
    )


if __name__ == "__main__":
    main()
