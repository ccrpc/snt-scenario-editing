# script to calculate block group average score
from dbUtil import dbUtil
import geopandas as geopd
import csv


BASELINE_PATH = '/home/kml42638@co.champaign.il.us/data/lrtp/baseline/access_data.gpkg'
PREFERRED = '/home/kml42638@co.champaign.il.us/data/preferred/access_data.gpkg'
PREFERRED_HSR = '/home/kml42638@co.champaign.il.us/data/preferred_hsr/access_data.gpkg'
BG_PATH = '/home/kml42638@co.champaign.il.us/data/lrtp/census_bg/tl_2018_17_bg.shp'


def area_score(area, intersections,
               modes=['bus', 'pedestrian', 'bicycle']):
    columns = []
    for mode in modes:
        columns.extend([col for col in intersections if mode in col])
    columns.append('geometry')
    intersections = intersections[columns]

    intersections['MEAN_SCORE'] = intersections.mean(axis=1)
    intersections = intersections[['MEAN_SCORE', 'geometry']]
    combined_df = geopd.sjoin(
        intersections, area, op='intersects', how="inner")
    combined_df = combined_df.groupby(['GEOID']).mean()
    combined_df = combined_df.reset_index()
    combined_df = combined_df[['GEOID', 'MEAN_SCORE']]
    return combined_df


def calculate_score(gpkg_path):
    baseline_access = dbUtil.read_gdb(gpkg_path, 'intersection')
    census_bg = dbUtil.read_shapefile(BG_PATH)
    census_bg = census_bg.to_crs({'init': 'epsg:4326'})
    census_bg = census_bg[['GEOID', 'geometry']]

    base_bike_job = area_score(census_bg, baseline_access, modes=['bicycle'])
    base_ped_job = area_score(census_bg, baseline_access, modes=['pedestrian'])
    base_bus_job = area_score(census_bg, baseline_access, modes=['bus'])
    base_car_job = area_score(census_bg, baseline_access, modes=['vehicle'])

    base_bike_job = dbUtil.rename_column(
        base_bike_job, {'MEAN_SCORE': 'base_bike_all_destinations'})
    base_ped_job = dbUtil.rename_column(
        base_ped_job, {'MEAN_SCORE': 'base_ped_all_destinations'})
    base_bus_job = dbUtil.rename_column(
        base_bus_job, {'MEAN_SCORE': 'base_bus_all_destinations'})
    base_car_job = dbUtil.rename_column(
        base_car_job, {'MEAN_SCORE': 'base_vehicle_all_destinations'})

    combined = dbUtil.join_df(base_bike_job, base_ped_job, 'GEOID', 'GEOID')
    combined = dbUtil.join_df(combined, base_bus_job, 'GEOID', 'GEOID')
    combined = dbUtil.join_df(combined, base_car_job, 'GEOID', 'GEOID')

    combined = dbUtil.convert_data_type(combined, 'GEOID', 'string')
    combined = dbUtil.convert_data_type(
        combined, 'base_bike_all_destinations', 'numeric')
    combined = dbUtil.convert_data_type(
        combined, 'base_ped_all_destinations', 'numeric')
    combined = dbUtil.convert_data_type(
        combined, 'base_bus_all_destinations', 'numeric')
    combined = dbUtil.convert_data_type(
        combined, 'base_vehicle_all_destinations', 'numeric')

    return combined


if __name__ == '__main__':
    base = calculate_score(BASELINE_PATH)
    preferred = calculate_score(PREFERRED)
    preferred_hsr = calculate_score(PREFERRED_HSR)

    base.to_csv('/home/kml42638@co.champaign.il.us/data/lrtp/baseline/baseline_bg_scores.csv',
                quoting=csv.QUOTE_NONNUMERIC)
    preferred.to_csv('/home/kml42638@co.champaign.il.us/data/lrtp/preferred/preferred_bg_scores.csv',
                     quoting=csv.QUOTE_NONNUMERIC)
    preferred_hsr.to_csv(
        '/home/kml42638@co.champaign.il.us/data/lrtp/preferred_hsr/preferred_hsr_bg_scores.csv',
        quoting=csv.QUOTE_NONNUMERIC)
