# extract the project segment back
import geopandas as gpd
import os
import pandas as pd


class Network:
    df = gpd.GeoDataFrame()

    def __init__(self, path, layer, df=None):
        if path is not None:
            self.df = gpd.read_file(
                path, driver="GPKG", layer=layer
            ).set_index("segment_id")
        else:
            self.df = df

    @classmethod
    def from_df(cls, df):
        return cls(path=None, layer=None, df=df)

    def get_diff(self, network, column):
        df = self.df[column].sub(network.df[column])
        return pd.merge(
            gpd.GeoDataFrame(self.df[["geometry", "name"]]),
            df,
            left_index=True,
            right_index=True,
        )

    def rename_column(self, col_dict):
        self.df = self.df.rename(columns=col_dict)

    def drop_columns(self, cols):
        self.df = self.df.drop(cols, axis=1)

    def __add__(self, other):
        return pd.merge(self.df, other.df, left_index=True, right_index=True)


base_path = "/mnt/data/"
baseline_path = "baseline/lts_score_combine.gpkg"


def main():
    projects = Network(
        "/mnt/git/snt-scenario-editing/src/merged_projects.gpkg",
        "future_projects",
    )

    baseline = Network(os.path.join(base_path, baseline_path), "result")
    import pdb

    pdb.set_trace()


if __name__ == "__main__":

    main()
