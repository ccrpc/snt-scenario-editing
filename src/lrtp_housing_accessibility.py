# Script to calculate score for LRTP performance measure
import env
from dbUtil import dbUtil
from shapely.ops import nearest_points


PUBLIC_HOUSING_SQL = """
SELECT id, ST_transform(geom, 4326) AS geom FROM housing.affordable
"""

BASELINE_PATH = '/mnt/data/lrtp/baseline/access_data.gpkg'
PREFERRED_PATH = '/mnt/data/lrtp/preferred/access_data.gpkg'
PREFERRED_HSR_PATH = '/mnt/data/lrtp/preferred_hsr/access_data.gpkg'


def nearest_score(
        poi,
        intersections,
        modes=['bus', 'pedestrian', 'bicycle'],
        score_col_name='score'):
    # poi = gdf, intersection = gdf
    columns = []
    for mode in modes:
        columns.extend([col for col in intersections if mode in col])
    columns.append('geometry')
    intersections = intersections[columns]
    intersections['MEAN_SCORE'] = intersections.mean(axis=1)

    intersections_multipoint = intersections.geometry.unary_union

    def near(point, pts=intersections_multipoint):
        nearest = intersections.geometry == nearest_points(point, pts)[1]
        return intersections[nearest].MEAN_SCORE.get_values()[0]

    poi[score_col_name] = poi.apply(lambda row: near(row.geom), axis=1)

    return poi


def mean(df):
    df = df.set_index('id')
    df['mean'] = df.mean(axis=1)
    return df


def baseline(housing):
    baseline_access = dbUtil.read_gdb(BASELINE_PATH, 'intersection')
    nearest_score(
        housing, baseline_access,
        modes=['pedestrian'], score_col_name='pedestrian')
    nearest_score(
        housing, baseline_access,
        modes=['bicycle'], score_col_name='bicycle')
    nearest_score(
        housing, baseline_access,
        modes=['bus'], score_col_name='bus')
    nearest_score(
        housing, baseline_access,
        modes=['vehicle'], score_col_name='vehicle')
    housing = mean(housing)
    return housing


def preferred(housing):
    preferred_access = dbUtil.read_gdb(PREFERRED_PATH, 'intersection')
    nearest_score(
        housing, preferred_access,
        modes=['pedestrian'], score_col_name='pedestrian')
    nearest_score(
        housing, preferred_access,
        modes=['bicycle'], score_col_name='bicycle')
    nearest_score(
        housing, preferred_access,
        modes=['bus'], score_col_name='bus')
    nearest_score(
        housing, preferred_access,
        modes=['vehicle'], score_col_name='vehicle')
    housing = mean(housing)
    return housing


def preferred_hsr(housing):
    preferred_hsr_access = dbUtil.read_gdb(PREFERRED_HSR_PATH, 'intersection')
    nearest_score(
        housing, preferred_hsr_access,
        modes=['pedestrian'], score_col_name='pedestrian')
    nearest_score(
        housing, preferred_hsr_access,
        modes=['bicycle'], score_col_name='bicycle')
    nearest_score(
        housing, preferred_hsr_access,
        modes=['bus'], score_col_name='bus')
    nearest_score(
        housing, preferred_hsr_access,
        modes=['vehicle'], score_col_name='vehicle')
    housing = mean(housing)
    return housing


def to_csv(df, path):
    df = df.drop(columns=['geom'])
    df.to_csv(path)


if __name__ == '__main__':
    housing = dbUtil.get_gdb(env.DB, PUBLIC_HOUSING_SQL)
    baseline = baseline(housing.copy())
    preferred = preferred(housing.copy())
    preferred_hsr = preferred_hsr(housing.copy())

    to_csv(baseline, '/mnt/data/lrtp/baseline/baseline_housing.csv')
    to_csv(preferred, '/mnt/data/lrtp/preferred/preferred_housing.csv')
    to_csv(preferred_hsr, '/mnt/data/lrtp/preferred_hsr/preferred_hsr_housing.csv')
