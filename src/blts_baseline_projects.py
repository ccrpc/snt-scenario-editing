# compare baseline attributes and project attribute and see if the attributes
# got better or worse
from dbUtil import dbUtil
import geopandas as gpd
import config as c
import env
import psycopg2
import pandas as pd
import numpy as np
import os

pd.set_option("display.max_rows", 500)
# pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 400)

# blts_cols = [
#     "aadt",*
#     "volume_capacity",
#     "bicycle_buffer_width",
#     "bicycle_facility_width",
#     "parking_lane_width",
# ]

blts_cols = [
    "bicycle_buffer_width",
    "bicycle_facility_width",
    "parking_lane_width",
]


def clean_future_projects():
    # getting the future projects clean up
    df = gpd.read_file(
        "bike_projects.gpkg", driver="GPKG", layer="BikeProjects_finaledits"
    )
    df = dbUtil.rename_column(df, c.SANITIZE)

    ori = df.filter(regex=("^((?!data-1567615426275).)*$|segment_id"))
    new = df.filter(regex=("data-1567615426275.*|segment_id"))
    new = dbUtil.rename_column(new, c.REMOVE)

    ori = ori.set_index("segment_id")
    new = new.set_index("segment_id")
    ori.update(new)

    ori = ori.reset_index()
    ori.loc[
        ori["pavement_condition"] == "100", "pavement_condition"
    ] = "Excellent"

    to_numeric = [
        "bicycle_buffer_width",
        "bicycle_facility_width",
        "sidewalk_condition_score",
        "sidewalk_buffer_width",
        "sidewalk_width",
    ]
    for col in to_numeric:
        ori = dbUtil.convert_data_type(ori, col, "numeric")

    ori = ori.set_index("segment_id")
    return ori


def get_network(connection, query, geom_col="geom"):
    with psycopg2.connect(**connection) as conn:
        if geom_col:
            network_df = gpd.GeoDataFrame.from_postgis(
                query, conn, geom_col=geom_col
            )
    return network_df


def blts_diff(df):
    # blts_a = df.query("bicycle_buffer_width<0 or bicycle_facility_width<0 or parking_lane_width<0")
    # blts_b = df.query("aadt>0 or volume_capacity>0")
    # index = blts_a.index.union(blts_b.index)

    blts = df.query("bicycle_buffer_width<0 or bicycle_facility_width<0 or parking_lane_width<0")
    return blts


def project_name():
    df = gpd.read_file(
        "bike_projects.gpkg", driver="GPKG", layer="BikeProjects_finaledits"
    )
    df = df[['data-1567615426275_name', 'segment_id']]
    df = df.rename(columns={'data-1567615426275_name': 'project_name'})
    df = df.set_index('segment_id')
    return df


if __name__ == "__main__":
    future = clean_future_projects()
    network = get_network(env.DB, c.NETWORK_SQL)
    project_name = project_name()

    future_quan = future[blts_cols]
    network_quan = network[blts_cols]
    network_quan = network_quan.fillna(0)
    diff = future_quan.sub(network_quan, axis="index")
    diff = diff.loc[future.index]
    blts = blts_diff(diff)

    # compare sidewalk buffer
    network = network.replace(np.nan, 'none', regex=True)
    future = future.replace(np.nan, 'none', regex=True)
    future = future.replace('', 'none', regex=True)
    network = network.replace('', 'none', regex=True)
    future = future.replace(' ', 'none')
    network = network.replace(' ', 'none')

    bicycle_path_category_diff = future.bicycle_path_category.str.lower() != network.loc[future.index].bicycle_path_category.str.lower()
    bicycle_path_category_diff = network.loc[future.index][bicycle_path_category_diff]['bicycle_path_category'].str.lower() + '>' + future[bicycle_path_category_diff]['bicycle_path_category'].str.lower()
    bicycle_path_category_diff = bicycle_path_category_diff[~bicycle_path_category_diff.str.contains('>none')]
    bicycle_path_category_diff = bicycle_path_category_diff[~bicycle_path_category_diff.str.contains('none>')]
    bicycle_path_category_diff = bicycle_path_category_diff[~bicycle_path_category_diff.str.contains('>off-street trail')]

    bicycle_buffer_type_diff = future.bicycle_buffer_type.str.lower() != network.loc[future.index].bicycle_buffer_type.str.lower()
    bicycle_buffer_type_diff = network.loc[future.index][bicycle_buffer_type_diff]['bicycle_buffer_type'].str.lower() + '>' + future[bicycle_buffer_type_diff]['bicycle_buffer_type'].str.lower()
    bicycle_buffer_type_diff = bicycle_buffer_type_diff[~bicycle_buffer_type_diff.str.contains('>none')]
    bicycle_buffer_type_diff = bicycle_buffer_type_diff[~bicycle_buffer_type_diff.str.contains('none>')]
    bicycle_buffer_type_diff = bicycle_buffer_type_diff[~bicycle_buffer_type_diff.str.contains('not applicable>')]
    bicycle_buffer_type_diff = bicycle_buffer_type_diff[~bicycle_buffer_type_diff.str.contains('>landscaped with trees')]

    blts = blts.merge(bicycle_path_category_diff, how='outer', left_index=True, right_index=True)
    blts = blts.merge(bicycle_buffer_type_diff, how='outer', left_index=True, right_index=True)

    # join project name
    blts = blts.merge(project_name, how='left', left_index=True, right_index=True)
    blts = blts.sort_values(by='project_name')
    import pdb; pdb.set_trace()
