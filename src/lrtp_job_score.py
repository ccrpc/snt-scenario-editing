# script to calculate block group average score
from dbUtil import dbUtil
import geopandas as geopd
import csv


BASELINE_PATH = '/mnt/data/lrtp/baseline/access_data.gpkg'
PREFERRED_PATH = '/mnt/data/lrtp/preferred/access_data.gpkg'
PREFERRED_HSR_PATH = '/mnt/data/lrtp/preferred_hsr/access_data.gpkg'
BG_PATH = '/mnt/data/lrtp/census_bg/tl_2018_17_bg.shp'


def area_score(area, intersections, modes=['bus', 'pedestrian', 'bicycle']):
    columns = []
    for mode in modes:
        columns.extend([col for col in intersections if mode in col])
    columns.append('geometry')
    intersections = intersections[columns]
    intersections['MEAN_SCORE'] = intersections.mean(axis=1)
    intersections = intersections[['MEAN_SCORE', 'geometry']]
    combined_df = geopd.sjoin(
        intersections, area, op='intersects', how="inner")
    combined_df = combined_df.groupby(['GEOID']).mean()
    combined_df = combined_df.reset_index()
    combined_df = combined_df[['GEOID', 'MEAN_SCORE']]
    return combined_df


def calculate_score(PATH):
    df = dbUtil.read_gdb(PATH, 'intersection')
    census_bg = dbUtil.read_shapefile(BG_PATH)
    census_bg = census_bg.to_crs({'init': 'epsg:4326'})
    census_bg = census_bg[['GEOID', 'geometry']]

    bike_job = area_score(census_bg, df, modes=['bicycle_job'])
    ped_job = area_score(census_bg, df, modes=['pedestrian_job'])
    bus_job = area_score(census_bg, df, modes=['bus_job'])
    car_job = area_score(census_bg, df, modes=['vehicle_job'])

    base_bike_job = dbUtil.rename_column(bike_job, {'MEAN_SCORE': 'bike_job'})
    base_ped_job = dbUtil.rename_column(ped_job, {'MEAN_SCORE': 'ped_job'})
    base_bus_job = dbUtil.rename_column(bus_job, {'MEAN_SCORE': 'bus_job'})
    base_car_job = dbUtil.rename_column(car_job, {'MEAN_SCORE': 'vehicle_job'})

    combined = dbUtil.join_df(base_bike_job, base_ped_job, 'GEOID', 'GEOID')
    combined = dbUtil.join_df(combined, base_bus_job, 'GEOID', 'GEOID')
    combined = dbUtil.join_df(combined, base_car_job, 'GEOID', 'GEOID')
    return combined


if __name__ == '__main__':
    baseline = calculate_score(BASELINE_PATH)
    preferred = calculate_score(PREFERRED_PATH)
    preferred_hsr = calculate_score(PREFERRED_HSR_PATH)

    baseline.to_csv('/mnt/data/lrtp/baseline/baseline_job_accessibility.csv',
                    quoting=csv.QUOTE_NONNUMERIC)
    preferred.to_csv(
        '/mnt/data/lrtp/preferred/preferred_job_accessibility.csv',
        quoting=csv.QUOTE_NONNUMERIC)
    preferred_hsr.to_csv(
        '/mnt/data/lrtp/preferred_hsr/preferred_hsr_job_accessibility.csv',
        quoting=csv.QUOTE_NONNUMERIC)
