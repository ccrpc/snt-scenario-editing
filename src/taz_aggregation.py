# script to calculate block group average score
from dbUtil import dbUtil
import geopandas as gpd
import csv
import env
import pandas as pd


BASELINE_PATH = '/mnt/data/lrtp/baseline/access_data.gpkg'
PREFERRED = '/mnt/data/lrtp/preferred/access_data.gpkg'
PREFERRED_HSR = '/mnt/data/lrtp/preferred_hsr/access_data.gpkg'
TAZ_PATH = '/mnt/data/lrtp/census_bg/tl_2018_17_bg.shp'
OUT_PATH = '/mnt/data/hia/taz_aggregation.gpkg'
TAZ_SQL = """
    SELECT id, geom, taz_type
        FROM boundary.traffic_analysis_zone
"""
taz_df = dbUtil.get_gdb(env.DB, TAZ_SQL)
il_proj = {'init': 'epsg:3435'}
buffer_distance = 20


def reproject(gdf, to_proj):
    return gdf.to_crs(to_proj)


def area_score(area, intersections):
    area = area.set_index('id')
    area['geom'] = area.buffer(buffer_distance)

    combined_df = gpd.sjoin(
        intersections, area, op='intersects', how="inner")

    combined_df = combined_df.groupby(['index_right']).mean()
    combined_df = combined_df.reset_index()
    combined_df = dbUtil.rename_column(
        combined_df, {'index_right': 'id'})
    combined_df = combined_df.set_index('id')

    combined_df = pd.merge(combined_df, area, on='id', how='inner')
    combined_df = gpd.GeoDataFrame(combined_df, geometry='geom')
    combined_df.crs = il_proj
    combined_df = reproject(combined_df, {'init': 'epsg:4326'})
    return combined_df


def calculate_score(gpkg_path):
    gdf = dbUtil.read_gdb(gpkg_path, 'intersection')
    gdf = reproject(gdf, il_proj)
    combined = area_score(taz_df, gdf)
    return combined


if __name__ == '__main__':
    base = calculate_score(BASELINE_PATH)
    preferred = calculate_score(PREFERRED)
    preferred_hsr = calculate_score(PREFERRED_HSR)

    base = base.reset_index()
    preferred = preferred.reset_index()
    preferred_hsr = preferred_hsr.reset_index()

    base.to_file(OUT_PATH, layer='base', driver="GPKG")
    preferred.to_file(OUT_PATH, layer='preferred', driver="GPKG")
    preferred_hsr.to_file(OUT_PATH, layer='preferred_hsr', driver="GPKG")
