from dbUtil import dbUtil
import geopandas as gpd
import psycopg2
import env
import pandas as pd
import config as c


def get_network(connection, query, geom_col='geom'):
    with psycopg2.connect(**connection) as conn:
        if geom_col:
            network_df = gpd.GeoDataFrame.from_postgis(query,
                                                       conn,
                                                       geom_col=geom_col)
    return network_df


def clean_future_projects():
    # getting the future projects clean up
    df = gpd.read_file('bike_projects.gpkg',
                       driver='GPKG',
                       layer='BikeProjects_finaledits')
    df = dbUtil.rename_column(df, c.SANITIZE)

    ori = df.filter(regex=('^((?!data-1567615426275).)*$|segment_id'))
    new = df.filter(regex=('data-1567615426275.*|segment_id'))
    new = dbUtil.rename_column(new, c.REMOVE)

    ori = ori.set_index('segment_id')
    new = new.set_index('segment_id')
    ori.update(new)

    ori = ori.reset_index()
    ori.loc[ori['pavement_condition'] == '100', 'pavement_condition'] = 'Excellent'

    ori.to_file('merged_projects.gpkg', layer='future_projects', driver="GPKG")
    ori = ori.set_index('segment_id')
    return ori


def main():
    future_projects = clean_future_projects()
    network = get_network(env.DB, c.NETWORK_SQL)
    network = network.set_index('segment_id')
    test = network.copy()
    network.update(future_projects)
    network = network.reset_index()
    assert not pd.DataFrame.equals(network, test)

    network.to_file(
        'future_network.gpkg', layer='future_network', driver='GPKG')


if __name__ == '__main__':
    main()
