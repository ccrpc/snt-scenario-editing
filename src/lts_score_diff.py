# Compare the score difference for baseline and scenario
import geopandas as gpd
import os
from dbUtil import dbUtil


def load_network(path, layer="result"):
    df = gpd.read_file(path, driver="GPKG", layer=layer)
    return df


def get_projects():
    project_path = "./bike_projects.gpkg"
    layer = "BikeProjects_finaledits"
    gdf = gpd.read_file(project_path, driver="GPKG", layer=layer)
    gdf = gdf[["data-1567615426275_name", "segment_id", "geometry"]]
    gdf = gdf.rename(columns={"data-1567615426275_name": "name"})
    return gdf


def get_network():
    network_path = "./merged_projects.gpkg"
    layer = "future_projects"
    gdf = gpd.read_file(network_path, driver="GPKG", layer=layer)
    gdf = gdf.set_index("segment_id")
    return gdf


if __name__ == "__main__":
    BASELINE = "/mnt/data/baseline/lts_score_combine.gpkg"
    PREFERRED = "/mnt/data/preferred/lts_score.gpkg"
    print("Loading Networks...")
    baseline = load_network(BASELINE)
    preferred = load_network(PREFERRED)

    baseline = baseline.set_index("segment_id")
    preferred = preferred.set_index("segment_id")

    network = baseline["geometry"]
    network.index = baseline.index
    network = gpd.GeoDataFrame(network)
    network["blts_diff"] = baseline["blts_score"].sub(preferred["blts_score"])
    network["plts_diff"] = baseline["plts_score"].sub(preferred["plts_score"])
    network["alts_diff"] = baseline["alts_score"].sub(preferred["alts_score"])

    network = network.reset_index()
    projects = get_projects()
    projects = projects[["name", "segment_id"]]
    merged_df = dbUtil.join_df(network, projects, "segment_id", "segment_id")
    merged_df.to_file(
        "/mnt/data/statistic/score_diff.gpkg", layer="diff", driver="GPKG"
    )

    count = merged_df.groupby("name").count()["segment_id"]
    count.to_csv("./project_count.csv")

    network = network.set_index("segment_id")
    network = network.drop(columns=["geometry"])
    future_projects = get_network()
    future_projects = future_projects.merge(
        network, left_index=True, right_index=True, how="left"
    )
    import pdb

    pdb.set_trace()

