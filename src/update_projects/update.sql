-- Drop scenario table

DROP TABLE IF EXISTS lrtp.scenario_change CASCADE;
DROP TABLE IF EXISTS ltrp.scenario_network CASCADE;
DROP TABLE IF EXISTS lrtp.scenario_project CASCADE;


-- JOIN project changes to project geometry
DROP MATERIALIZED VIEW IF EXISTS lrtp.future_projects ;
CREATE MATERIALIZED VIEW lrtp.future_projects AS (
  WITH path_single AS (
    SELECT
      (ST_Dump(paths.geom)).geom AS geom,
      name
    FROM lrtp.bike_paths AS paths
  )
  SELECT
    bike_paths.name,
    changes.bicycle_buffer_type,
    changes.bicycle_path_category,
    changes.bicycle_path_type,
    changes.bicycle_approach_alignment,
    changes.bicycle_buffer_width,
    changes.bicycle_facility_width,
    changes.lane_configuration,
    changes.lanes_per_direction,
    changes.parking_lane_width,
    changes.right_turn_length,
    changes.sidewalk_buffer_type,
    changes.sidewalk_buffer_width,
    changes.sidewalk_condition_score,
    changes.sidewalk_width,
    changes.pavement_condition,
    changes.posted_speed,
    changes.total_lanes,
    bike_paths.geom
  FROM lrtp.changes AS changes
  JOIN path_single AS bike_paths
    ON ST_DWithin(changes.geom, bike_paths.geom, 100) AND
      pcd_segment_match(changes.geom, bike_paths.geom, 100)
)

