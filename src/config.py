
SANITIZE = {
    'bus_stop_c': 'bus_trips_total',
    'crossing_s': 'crossing_speed',
    'heavy_vehi': 'heavy_vehicle_count',
    'max_lanes_': 'max_lanes_crossed',
    'right_turn': 'right_turn_length',
    'crossing_a': 'crossing_aadt',
    'bicycle_bu': 'bicycle_buffer_width',
    'bicycle_fa': 'bicycle_facility_width',
    'functional': 'functional_classificiation',
    'parking_la': 'parking_lane_width',
    'sidewalk_b': 'sidewalk_buffer_width',
    'sidewalk_c': 'sidewalk_condition_score',
    'sidewalk_w': 'sidewalk_width',
    'volume_cap': 'volume_capacity',
    'crossing_f': 'crossing_functional_classification',
    'cross_name': 'cross_name_start',
    'cross_na_1': 'cross_name_end',
    'bicycle__1': 'bicycle_buffer_type',
    'bicycle_pa': 'bicycle_path_category',
    'bicycle__2': 'bicycle_path_type',
    'bicycle_ap': 'bicycle_approach_alignment',
    'intersecti': 'intersection_control_type',
    'intersec_1': 'intersection_median_refuge',
    'lane_confi': 'lane_configuration',
    'lanes_per_': 'lanes_per_direction',
    'overall_la': 'overall_land_use',
    'pavement_c': 'pavement_condition',
    'posted_spe': 'posted_speed',
    'railroad_c': 'railroad_crossing_type',
    'road_sign_': 'road_sign_type',
    'sidewalk_1': 'sidewalk_buffer_type',
    'start_inte': 'start_intersection_id',
    'end_inters': 'end_intersection_id'
}

REMOVE = {
    'data-1567615426275_name': 'name',
    'data-1567615426275_bicycle_buffer_width': 'bicycle_buffer_width',
    'data-1567615426275_bicycle_facility_width': 'bicycle_facility_width',
    'data-1567615426275_bicycle_buffer_type': 'bicycle_buffer_type',
    'data-1567615426275_bicycle_path_category': 'bicycle_path_category',
    'data-1567615426275_bicycle_path_type': 'bicycle_path_type',
    'data-1567615426275_sidewalk_buffer_width': 'sidewalk_buffer_width',
    'data-1567615426275_sidewalk_condition_score': 'sidewalk_condition_score',
    'data-1567615426275_sidewalk_width': 'sidewalk_width',
    'data-1567615426275_sidewalk_buffer_type': 'sidewalk_buffer_type',
    'data-1567615426275_pavement_condition': 'pavement_condition',
}

NETWORK_SQL = """
    SELECT * FROM lrtp.scenario_network
"""
