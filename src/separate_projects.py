# This script is used to seprate the projects from the segment
import geopandas as gpd
import pandas as pd
import numpy as np


class Network:
    df = gpd.GeoDataFrame()

    def __init__(self, path, layer, df=None):
        if path is not None:
            self.df = gpd.read_file(
                path, driver="GPKG", layer=layer
            ).set_index("segment_id")
        else:
            self.df = df

    @classmethod
    def from_df(cls, df):
        return cls(path=None, layer=None, df=df)

    def get_diff(self, network, column):
        df = self.df[column].sub(network.df[column])
        return pd.merge(
            gpd.GeoDataFrame(self.df[["geometry", "name"]]),
            df,
            left_index=True,
            right_index=True,
        )

    def rename_column(self, col_dict):
        self.df = self.df.rename(columns=col_dict)

    def drop_columns(self, cols):
        self.df = self.df.drop(cols, axis=1)

    def compare(self, other, columns):
        pass

    def __add__(self, other):
        return pd.merge(self.df, other.df, left_index=True, right_index=True)


class Projects(Network):
    def __init__(self, path, layer):
        super().__init__(path, layer)
        self.df = (
            self.df.replace(r"^\s*$", np.NaN, regex=True)
            .fillna(value=np.NaN)
            .drop(columns=["geometry"])
            .apply(pd.to_numeric, errors="ignore")
        )

    def _convert_to_numeric(self):
        self.df


if __name__ == "__main__":
    FUTURE_PROJECTS = "/mnt/git/snt-scenario-editing/src/merged_projects.gpkg"
    BASELINE = "/mnt/data/baseline/lts_score_combine.gpkg"

    projects = Projects(FUTURE_PROJECTS, "future_projects")
    projects.rename_column({"total_lane": "total_lanes"})

    baseline = Network(BASELINE, "result")

    comparison_columns = [
        "bicycle_buffer_type",
        "bicycle_path_category",
        "bicycle_path_type",
        "bicycle_approach_alignment",
        "bicycle_buffer_width",
        "bicycle_facility_width",
        "lane_configuration",
        "lanes_per_direction",
        "parking_lane_width",
        "right_turn_length",
        "sidewalk_buffer_type",
        "sidewalk_buffer_width",
        "sidewalk_condition_score",
        "sidewalk_width",
        "pavement_condition",
        "posted_speed",
        "total_lanes",
    ]

    df_bool = (
        baseline.df.loc[projects.df.index][comparison_columns]
        != projects.df[comparison_columns]
    )
    changes = projects.df[df_bool][comparison_columns]

    segment_geom = gpd.GeoDataFrame(baseline.df[["geometry", "name"]])
    merged = gpd.GeoDataFrame(
        pd.merge(
            changes,
            segment_geom,
            how="left",
            left_index=True,
            right_index=True,
        )
    )
    merged = merged.reset_index()
    merged["issue"] = ""
    merged.crs = {"init": "epsg:4326"}
    import pdb

    pdb.set_trace()
    merged.to_file(
        "/mnt/data/projects/project_changes.gpkg",
        layer="changes",
        driver="GPKG",
    )

