import subprocess
import env
import geopandas as gpd


config = \
"""
{
  "segment": {
    "description": "Difference between baseline and scenario",
    "minzoom": 0,
    "maxzoom": 14
  },
  "destination": {
    "description": "all destinations locations",
    "minzoom": 0,
    "maxzoom": 14
  },
  "project": {
      "description": "future scenario projects",
      "minzoom": 0,
      "maxzoom": 14
  },
}
"""


def gpkg_to_mbtiles(gpkg_in_path, output_path, **kwargs):
    cmd = ['ogr2ogr', '-f', 'MBTiles', output_path,
           gpkg_in_path]
    for key, value in kwargs.items():
        cmd.extend(['-dsco', '{}={}'.format(key, value)])
    subprocess.check_output(cmd)


def get_projects(path):
    project_path = path
    layer = 'BikeProjects_finaledits'
    gdf = gpd.read_file(project_path, driver='GPKG', layer=layer)
    gdf = gdf[['data-1567615426275_name', 'geometry']]
    gdf = gdf.rename(columns={'data-1567615426275_name': 'name'})
    return gdf


if __name__ == '__main__':
    projects = get_projects(env.projects_path)
    projects.to_file(env.preferred_path, layer='project', driver='GPKG')
    projects.to_file(env.preferred_hsr_path, layer='project', driver='GPKG')

    gpkg_to_mbtiles(
        gpkg_in_path=env.preferred_path,
        output_path=env.preferred_tile,
        NAME='mbtiles',
        DESCRIPTION='Score differences for baseline and scenarios',
        MINZOOM=0,
        MAXZOOM=14,
        CONF=config
    )

    gpkg_to_mbtiles(
        gpkg_in_path=env.preferred_hsr_path,
        output_path=env.preferred_hsr_tile,
        NAME='mbtiles',
        DESCRIPTION='Score differences for baseline and scenarios',
        MINZOOM=0,
        MAXZOOM=14,
        CONF=config
    )
