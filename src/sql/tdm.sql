
CREATE SCHEMA IF NOT EXISTS travel_demand_model;


CREATE TABLE travel_demand_model.link (
  id SERIAL PRIMARY KEY,
  link_id SMALLINT NOT NULL UNIQUE,
  start_node_a SMALLINT NOT NULL,
  end_node_b SMALLINT NOT NULL,
  year SMALLINT NOT NULL,
  geom GEOMETRY NOT NULL
);

CREATE TABLE travel_demand_model.output (
  id SERIAL PRIMARY KEY,
  link_id SMALLINT REFERENCES travel_demand_model.link(link_id),
  scenario VARCHAR(50)
)

CREATE DOMAIN travel_demand_model.output AS varchar(50)
CHECK (scenario IN (
  'preferred',
  'preferred_with_high_speed_rail'
));
