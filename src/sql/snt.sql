-- REPLACED BY QGIS Steps
-- Copy street.segment to lrtp.scenario_network
-- CREATE TABLE lrtp.scenario_network AS
-- TABLE street.segment;
-- -- Create pk for scenario_network
-- ALTER TABLE lrtp.scenario_network ADD PRIMARY KEY (segment_id);

-- Remove fid
ALTER TABLE lrtp.scenario_network
DROP COLUMN fid;

-- Create lrtp.project
CREATE TABLE lrtp.scenario_project (
  id SERIAL PRIMARY KEY,
  project_id VARCHAR(50),
  name VARCHAR(300) NOT NULL,
  agency VARCHAR(100) NOT NULL,
  source VARCHAR(100) NOT NULL,
  status VARCHAR(20),
  priority VARCHAR(10),
  description VARCHAR(500)
);
-- Add geometry column to project
SELECT AddGeometryColumn('lrtp', 'scenario_project', 'geom', 4326, 'MULTILINESTRING', 2)

-- Create lrtp.change
-- Copy data schema
CREATE TABLE lrtp.scenario_change AS
TABLE lrtp.scenario_network
WITH NO DATA;
-- Add project_id column
ALTER TABLE lrtp.scenario_change
ADD COLUMN id INT;
-- drop geom
ALTER TABLE lrtp.scenario_change
DROP COLUMN geom;

ALTER TABLE lrtp.scenario_change ADD COLUMN pk SERIAL PRIMARY KEY;

-- Add project_id FK constriant
ALTER TABLE lrtp.scenario_change
ADD CONSTRAINT project_change_project_id_fk FOREIGN KEY (id)
  REFERENCES lrtp.scenario_project (id);
-- Add project_id FK constriant
ALTER TABLE lrtp.scenario_change
ADD CONSTRAINT scenario_network_change_segment_id_fk FOREIGN KEY (segment_id)
  REFERENCES lrtp.scenario_network (segment_id);


-- Insert some test data for testing (testing)
INSERT INTO lrtp.scenario_project (project_id, name, agency, description, source, status, priority)
VALUES ('id1', 'awesome project', 'ccrpc', 'awesome description', 'lrtp', 'completed', 'low');
INSERT INTO lrtp.scenario_project (project_id, name, agency, description, source, status, priority)
VALUES ('id2', 'cool project', 'cumtd', 'cool description', 'lrtp', 'high');

INSERT INTO lrtp.scenario_change (id, segment_id, aadt, total_lanes)
VALUES (2, 1, 500, 5);
INSERT INTO lrtp.scenario_change (id, segment_id, total_lanes)
VALUES (3, 1, 6);
