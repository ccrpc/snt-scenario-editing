SELECT DISTINCT (project.id), project.name, change.*
FROM lrtp.scenario_change AS change
INNER JOIN lrtp.scenario_project AS project
  ON project.id = change.project_id


  SELECT
  	segment.segment_id,
  	future_project.name,
  	t.bicycle_buffer_width,
  	t.bicycle_facility_width,
      t.bicycle_buffer_type,
      t.bicycle_path_category,
      t.bicycle_path_type,
  	t.sidewalk_buffer_width,
    	t.sidewalk_condition_score,
    	t.sidewalk_width,
    	t.sidewalk_buffer_type,
    	t.pavement_condition
  FROM street.segment AS segment
  INNER JOIN bicycle.future_project AS future_project
    ON pcd_segment_match(segment.geom, ST_Transform(future_project.geom, 3435), 100)
  LEFT JOIN (
  	SELECT DISTINCT (project.id), project.name AS project_name, change.*
  	FROM lrtp.scenario_change AS change
  	INNER JOIN lrtp.scenario_project AS project ON project.id = change.project_id
  ) AS t ON t.project_name = future_project.name AND t.segment_id = segment.segment_id
  ORDER BY segment_id
