# script to merge tdm and scenario projects data
import pandas as pd
from dbUtil import dbUtil
import env
import psycopg2
import config as c
import geopandas as gpd
import subprocess


def process_tdm_foo(tdm_df, col_dict, volume_capacity='volume_capacity'):
    # filtering out facility 0
    tdm_df[tdm_df['FACILITY'] == 0]

    # Calculate volume_capacity
    twltl = col_dict.get('two_way_left_turn_lane')
    daily_traffic = col_dict.get('daily_traffic_volume_per_direction')
    capacity = col_dict.get('capacity')
    lanes = col_dict.get('lanes')

    tdm_df[volume_capacity] = 0
    tdm_df[volume_capacity].loc[tdm_df[twltl] == 1] = \
        (tdm_df[daily_traffic] * 0.1) \
        / (tdm_df[capacity] * tdm_df[lanes] * 1.3)
    tdm_df[volume_capacity].loc[tdm_df[twltl] == 0] = \
        (tdm_df[daily_traffic] * 0.1) \
        / (tdm_df[capacity] * tdm_df[lanes])

    # Data clean up
    filter_col = [value for value in col_dict.values()]
    filter_col.append(volume_capacity)
    tdm_df = dbUtil.filter_df(tdm_df, filter_col)

    rename_dict = dict((v, k) for k, v in col_dict.items())
    tdm_df = dbUtil.rename_column(tdm_df, rename_dict)
    return tdm_df


def process_tdm(df, col_dict):
    # filter out facility 0
    df = df[df['FACILITY'] != 0]
    # filter out object id of 0
    df = df[df['OBJECTID'] != 0]
    # filtering out all the other columns
    filter_col = [value for value in col_dict.values()]
    dbUtil.filter_df(df, filter_col)
    df = dbUtil.filter_df(df, filter_col)
    # rename columns
    rename_dict = dict((v, k) for k, v in col_dict.items())
    df = dbUtil.rename_column(df, rename_dict)

    return df


def get_network(connection, query, geom_col='geom'):
    with psycopg2.connect(**connection) as conn:
        if geom_col:
            network_df = gpd.GeoDataFrame.from_postgis(query,
                                                       conn,
                                                       geom_col=geom_col)
    return network_df


def clean_future_projects():
    # getting the future projects clean up
    df = gpd.read_file('/mnt/git/snt-scenario-editing/src/bike_projects.gpkg',
                       driver='GPKG',
                       layer='BikeProjects_finaledits')
    df = dbUtil.rename_column(df, c.SANITIZE)

    ori = df.filter(regex=('^((?!data-1567615426275).)*$|segment_id'))
    new = df.filter(regex=('data-1567615426275.*|segment_id'))
    new = dbUtil.rename_column(new, c.REMOVE)

    ori = ori.set_index('segment_id')
    new = new.set_index('segment_id')
    ori.update(new)

    ori = ori.reset_index()
    ori.loc[ori['pavement_condition'] == '100', 'pavement_condition'] = 'Excellent'

    to_numeric = [
        'bicycle_buffer_width',
        'bicycle_facility_width',
        'sidewalk_condition_score',
        'sidewalk_buffer_width',
        'sidewalk_width']
    for col in to_numeric:
        ori = dbUtil.convert_data_type(ori, col, 'numeric')

    ori = ori.set_index('segment_id')
    return ori


def set_up_dir():
    subprocess.check_output(['rm', '-rf', '/mnt/data/lrtp/preferred'])
    subprocess.check_output(['rm', '-rf', '/mnt/data/lrtp/preferred_hsr'])
    subprocess.check_output(['mkdir', '/mnt/data/lrtp/preferred'])
    subprocess.check_output(['mkdir', '/mnt/data/lrtp/preferred_hsr'])


def fix_permission():
    subprocess.check_output(['chown', '${USER}:${GROUP}', '/mnt/data/lrtp/preferred/preferred.gpkg'])
    subprocess.check_output(['chown', '${USER}:${GROUP}', '/mnt/data/lrtp/preferred_hsr/preferred_hsr.gpkg'])


def main():
    set_up_dir()
    # sql for getting the network
    network_sql = """
    SELECT * FROM lrtp.scenario_network
    """

    # read tdm table
    pre_df = pd.read_csv('/mnt/data/tdm/preferred.csv')
    hsr_df = pd.read_csv('/mnt/data/tdm/preferred_hsr.csv')

    match_table = pd.read_csv('/mnt/data/tdm/match_table.csv')
    # read network table from postgis db
    pre_network_df = dbUtil.get_gdb(env.DB, network_sql)

    # renamed bus stop to bus trips
    pre_network_df = dbUtil.rename_column(
        pre_network_df,
        {'bus_stop_count': 'bus_trips_total'})
    hsr_network_df = pre_network_df

    future_projects = clean_future_projects()
    network = get_network(env.DB, c.NETWORK_SQL)
    network = network.set_index('segment_id')
    test = network.copy()
    network.update(future_projects)
    assert not pd.DataFrame.equals(network, test)

    col_dict = {'tdm_id': 'OBJECTID',
                'volume_capacity': 'VC_1',
                'aadt': 'VT_1'
                }

    # clean up the data
    pre_df = process_tdm(pre_df, col_dict)
    hsr_df = process_tdm(hsr_df, col_dict)

    # join match table to tdm value
    pre_df = dbUtil.join_df(pre_df, match_table, 'tdm_id', 'objectid')
    hsr_df = dbUtil.join_df(hsr_df, match_table, 'tdm_id', 'objectid')

    # remove object id column
    filter_col = ['tdm_id', 'volume_capacity', 'aadt', 'segment_id']
    pre_df = dbUtil.filter_df(pre_df, filter_col)
    hsr_df = dbUtil.filter_df(hsr_df, filter_col)

    # set index before updatep
    pre_network_df = pre_network_df.set_index('segment_id')
    hsr_network_df = hsr_network_df.set_index('segment_id')
    pre_df = pre_df.dropna().set_index('segment_id')
    hsr_df = hsr_df.dropna().set_index('segment_id')

    # take the average for each segment
    pre_df = pre_df.groupby(level=0).mean()
    hsr_df = hsr_df.groupby(level=0).mean()

    # update network based on aadt and volume capacity from TDM
    test = pre_network_df.copy()
    pre_network_df.update(pre_df)
    hsr_network_df.update(hsr_df)
    assert not pd.DataFrame.equals(test, pre_network_df)

    # update network based on future projects
    test = pre_network_df.copy()
    pre_network_df.update(network)
    hsr_network_df.update(network)

    assert not pd.DataFrame.equals(test, pre_network_df)

    # resetting index
    pre_network_df = pre_network_df.reset_index()
    hsr_network_df = hsr_network_df.reset_index()

    # out put to geopackage
    pre_network_df.to_file(
        "/mnt/data/lrtp/preferred/preferred.gpkg", driver="GPKG")
    hsr_network_df.to_file(
        "/mnt/data/lrtp/preferred_hsr/preferred_hsr.gpkg", driver="GPKG")


if __name__ == '__main__':
    main()
