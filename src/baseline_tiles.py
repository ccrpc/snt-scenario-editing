import subprocess
import env

config = \
"""
{
  "network": {
    "description": "Difference between baseline and scenario",
    "minzoom": 0,
    "maxzoom": 14
  },
  "destination": {
    "description": "all destinations locations",
    "minzoom": 0,
    "maxzoom": 14
  }
}
"""


def gpkg_to_mbtiles(gpkg_in_path, output_path, **kwargs):
    cmd = ['ogr2ogr', '-f', 'MBTiles', output_path,
           gpkg_in_path]
    for key, value in kwargs.items():
        cmd.extend(['-dsco', '{}={}'.format(key, value)])
    subprocess.check_output(cmd)


gpkg_to_mbtiles(
    gpkg_in_path=env.baseline_package,
    output_path=env.baseline_tile,
    NAME='mbtiles',
    DESCRIPTION='Score differences for baseline and scenarios',
    MINZOOM=0,
    MAXZOOM=14,
    CONF=config
)
