import env
import psycopg2
import pandas as pd
import geopandas as gpd


class NetworkMerge:
    def __init__(self):
        print('network merge initiated!')

    @classmethod
    def get_change(cls, connection, sql):
        with psycopg2.connect(**connection) as conn:
            return pd.read_sql(sql, conn)

    def get_network(self, connection, query, geom_col='geom'):
        with psycopg2.connect(**connection) as conn:
            if geom_col:
                network_df = gpd.GeoDataFrame.from_postgis(query,
                                                           conn,
                                                           geom_col=geom_col)
                self.network_df = network_df

    def set_projects(self, projects):
        if not isinstance(projects, list):
            raise TypeError('projects must be a list of project df')
        self.projects_df = projects

    def merge_projects(self, method='simple', key='segment_id'):
        if method == 'simple':
            self._simple_merge(key)
        if method == 'priority':
            self._priority_merge(key)

    def to_geopackage(self, path, layer='combined'):
        if not isinstance(self.updated_df, pd.DataFrame):
            raise TypeError('Dataframe has not been merged')

        self.updated_df.to_file(
            path,
            driver='GPKG',
            layer=layer
        )

    def _simple_merge(self, key):
        network_df = self.network_df
        network_df = network_df.set_index([key])
        for df in self.projects_df:
            df = df.set_index([key])
            network_df.update(df)
        self.updated_df = network_df
        self.updated_df.reset_index()

    def _priority_merge(self, key):
        network_df = self.network_df
        projects_df = self.projects_df
        network_df = network_df.set_index([key])
        low = pd.DataFrame()
        medium = pd.DataFrame()
        high = pd.DataFrame()

        for project in projects_df:
            high = project[project['priority'] == 'high']
            high = high.groupby(['segment_id']).agg('first')
            medium = project[project['priority'] == 'medium']
            medium = medium.groupby(['segment_id']).agg('first')
            low = project[project['priority'] == 'low']
            low = low.groupby(['segment_id']).agg('first')

        if not low.empty:
            network_df.update(low)
        if not medium.empty:
            network_df.update(medium)
        if not high.empty:
            network_df.update(high)

        self.updated_df = network_df
        self.updated_df = self.updated_df.reset_index()


if __name__ == '__main__':
    NETWORK_SQL = """
        SELECT * FROM lrtp.scenario_network
    """

    CHANGE_SQL = """
        SELECT scenario_change.*, scenario_project.priority
        FROM lrtp.scenario_change
        JOIN lrtp.scenario_project ON
          scenario_change.project_id = scenario_project.id
    """

    PROJECTS = ['project 1']

    network = NetworkMerge()
    network.get_network(env.DB, NETWORK_SQL)
    projects_df = [NetworkMerge.get_change(env.DB,
                   CHANGE_SQL.format(project))
                   for project in PROJECTS]

    network.set_projects(projects_df)
    network.merge_projects(method='priority', key='segment_id')
    network.to_geopackage(env.GPKG_PATH)
